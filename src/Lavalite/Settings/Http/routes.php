<?php

// Admin routes for module
Route::group(['prefix' => trans_setlocale() . '/admin/settings'], function () {
    Route::get('cache/clear', 'SettingAdminWebController@flushCache');

    Route::resource('setting', 'SettingAdminWebController');
});
