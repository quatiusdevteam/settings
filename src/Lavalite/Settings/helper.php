<?php
 /**
 *	Admin Helper  
 */

if (!function_exists('setting'))
{
    function setting($key, $name=null, $default="")
    {
        return app('settings')->get($key, $name, $default);
    }
}

if (!function_exists('setting_set'))
{
    function setting_set($key, $name, $value="", $type="System")
    {
        app('settings')->set($key, $name, $value, $type);
    }
}

if (!function_exists('setting_save'))
{
    function setting_save($key, $name, $value="", $type="System")
    {
        app('settings')->set($key, $name, $value, $type);
    }
}

if (!function_exists('setting_remove'))
{
    function setting_remove($key, $name)
    {
        app('settings')->remove($key, $name);
    }
}

if (!function_exists('setting_unset'))
{
    function setting_unset($key, $name)
    {
        app('settings')->remove($key, $name);
    }
}